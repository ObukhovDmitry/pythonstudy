def insertion_sort(a, cmp=None):
    cmp = cmp or (lambda x, y: x < y)
    for i in range(1, len(a)):
        j = i
        while j > 0 and cmp(a[j], a[j - 1]):
            a[j - 1], a[j] = a[j], a[j - 1]
            j -= 1


def binary_search(a, x, cmp=None):
    l, r = 0, len(a)
    while l != r:
        m = (l + r) // 2
        if cmp(a[m], x):
            l = m + 1
        else:
            r = m
    return r


def binary_insertion_sort(a, cmp=None):
    cmp = cmp or (lambda x, y: x < y)
    for i in range(1, len(a)):
        j = binary_search(a[:i], a[i], cmp)
        if j != i:
            a[0:len(a)+1] = a[:j] + a[i:i+1] + a[j:i] + a[i+1:]


a = [(1, 'a'), (4, 'b'), (2, 'c'), (5, 'd'), (3, 'e'), (5, 'f'), (7, 'g')]
print(a)

binary_insertion_sort(a, cmp=lambda x, y: x[0] < y[0])
print(a)


