n = int(input())

print('begin')
print(n)

while n > 0:
    n -= 1
    print(n)

    if n == 7:
        print('FIRE 7')
        break

    if n == 20:
        print('FIRE 20')
        break

else:
    print('else block')

print('end')
