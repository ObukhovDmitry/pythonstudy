x = 0b1010001
y = 0o734214
z = 0x02a7eD
t = int('Z3F', base=36)

print(x)
print(y)
print(z)
print(t)

print()

print(bin(t))
print(oct(t))
print(hex(t))
