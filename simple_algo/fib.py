def fib_gen(n:int = -1):
    if n == 0:
        raise StopIteration
    a, b = 0, 1
    while n != 0:
        yield b
        a, b = b, a + b
        n -= 1


for i in fib_gen(15):
    print(i)
