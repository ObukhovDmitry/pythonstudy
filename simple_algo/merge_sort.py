def merge(a, l, m, r):
    i, j = l, m
    b = []

    while i < m and j < r:
        if a[j] < a[i]:
            b.append(a[j])
            j += 1
        else:
            b.append(a[i])
            i += 1

    b.extend(a[i:m])
    b.extend(a[j:r])

    a[l:r] = b


def merge_sort(a, l=0, r=None):
    r = r or len(a)
    if r - l < 2:
        return
    m = l + (r - l) // 2
    merge_sort(a, l, m)
    merge_sort(a, m, r)
    merge(a, l, m, r)


a = [1, 4, 2, 5, 3]
print(a)

merge_sort(a)
print(a)
