def f(n):
    assert n >= 0, 'n is negative!'
    res = 1
    for i in range(2, n + 1):
        res *= i
    return res


for i in range(10):
    print(i, f(i))

print(f(-1))
