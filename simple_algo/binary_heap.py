from heapq import heappush, heappop, heapify

class BinaryHeap:

    def __init__(self):
        self._heap = []

    def insert_key(self, k):
        heappush(self._heap, k)

    def remove(self, i):
        self.decrease_key(i, float("-inf"))
        self.extract_min()

    def get_min(self):
        return self._heap[0]

    def extract_min(self):
        return heappop(self._heap)

    def decrease_key(self, i, k_new):
        self._heap[i] = k_new
        p = (i-1)//2
        while i > 0 and self._heap[p] > self._heap[i]:
            self._heap[p], self._heap[i] = self._heap[i], self._heap[p]
            i, p = p, (i-1)//2


h = BinaryHeap()
h.insert_key(2)
h.insert_key(4)
h.insert_key(7)
h.insert_key(5)
h.insert_key(3)

print(h._heap)

h.decrease_key(2, 1)
print(h._heap)

print(h.extract_min())
print(h.get_min())
print(h.get_min())
print(h.extract_min())
print(h.extract_min())


